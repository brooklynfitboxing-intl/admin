// Configuration for your app

const vars = {
  production: {
    authUrl: 'https://myh4c.brooklynfitzone.com/auth',
    adminService: 'https://admin.brooklynfitzone.com/api',
    fwgService: 'https://brooklynfitzone.com/fwg',
    iotUrl: 'https://brooklynfitzone.com/iot/iot'
  },
  development: {
    authUrl: 'https://bfi.eu.ngrok.io/auth',
    adminService: 'https://bfi.eu.ngrok.io/admin-service',
    fwgService: 'https://bfi.eu.ngrok.io/fwg',
    iotUrl: 'https://bfi.eu.ngrok.io/iot/iot'
  }
}
console.log(vars[process.env.NODE_ENV || 'development'])
const env = Object.entries(vars[process.env.NODE_ENV || 'development']).reduce((acc, item) => {
  acc[item[0]] = JSON.stringify(item[1])
  return acc
}, {})
module.exports = function (ctx) {
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    boot: [
      'i18n',
      'axios',
      'auth',
      'apollo'
    ],

    css: [
      'app.styl'
    ],

    extras: [
      'roboto-font',
      'material-icons' // optional, you are not bound to it
      // 'ionicons-v4',
      // 'mdi-v3',
      // 'fontawesome-v5',
      // 'eva-icons'
    ],
    framework: {
      // all: true, // --- includes everything; for dev only!

      components: [
        'QLayout',
        'QHeader',
        'QDrawer', 'QScrollArea',
        'QPage', 'QPageContainer', 'QPageSticky',
        'QFooter',
        'QToolbar', 'QToolbarTitle',
        'QBtn', 'QBtnGroup', 'QBtnDropdown', 'QFab', 'QFabAction', 'QBtnToggle',
        'QIcon', 'QImg',
        'QList',
        'QItem', 'QItemSection', 'QItemLabel', 'QSeparator', 'QExpansionItem', 'QTime',
        'QAvatar',
        'QTable', 'QTh', 'QTr', 'QTd', 'QMarkupTable',
        'QCard', 'QCardSection', 'QCardActions',
        'QBanner',
        'QLinearProgress', 'QCircularProgress',
        'QBadge', 'QChip',
        'QTabs', 'QTab', 'QTabPanels', 'QTabPanel', 'QRouteTab',
        'QDialog', 'QBar', 'QTooltip', 'QSpace',
        'QForm', 'QInput', 'QDate', 'QSelect', 'QToggle', 'QCheckbox', 'QRadio',
        'QMenu',
        'QSpinner', 'QSpinnerDots',
        'QPopupProxy',
        'QCarousel', 'QCarouselSlide', 'QVideo',
        'QStepper', 'QStep', 'QStepperNavigation',
        'QSlideTransition', 'QSplitter',
        'QTimeline', 'QTimelineEntry',
        // Other
        'QUploader',
        'QPagination',
        'QPullToRefresh', 'QSkeleton', 'QRating',
        'QInfiniteScroll', 'QResponsive', 'QIntersection',
        'QInnerLoading'
      ],

      directives: [
        'Ripple',
        'ClosePopup'
      ],

      // Quasar plugins
      plugins: [
        'Notify', 'Loading', 'LocalStorage', 'Dialog', 'LoadingBar'
      ],

      config: {
        loadingBar: {
          color: 'secondary',
          size: '10px',
          position: 'top'
        }
      }

      // iconSet: 'ionicons-v4'
      // lang: 'de' // Quasar language
    },

    supportIE: false,

    build: {
      env,
      scopeHoisting: true,
      // vueRouterMode: 'history',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/
        })
      }
    },

    devServer: {
      // https: true,
      port: 8080,
      open: true // opens browser window automatically
    },

    // animations: 'all', // --- includes all animations
    animations: 'all',

    ssr: {
      pwa: false
    },

    pwa: {
      workboxPluginMode: 'InjectManifest',
      // workboxOptions: {}, // only for NON InjectManifest
      manifest: {
        name: 'Admin Hit4Change',
        short_name: 'MyH4C',
        // description: 'Best PWA App in town!',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#000',
        theme_color: '#000',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },

    cordova: {
      // id: 'org.cordova.quasar.app'
      // noIosLegacyBuildFlag: true // uncomment only if you know what you are doing
    },

    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack (cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'quasar-app'
      }
    }
  }
}
