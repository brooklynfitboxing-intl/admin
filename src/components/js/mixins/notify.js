export default {
  methods: {
    notifyDone (message) {
      this.$q.notify({
        message: message || this.$t('common.saved'),
        position: 'bottom-left',
        icon: 'done'
      })
    },
    notifyError (ex, timeout = 5000) {
      this.$q.notify({
        icon: 'error',
        message: (ex.response ? ex.response.data.message : ex.toString()),
        position: 'top',
        color: 'negative',
        closeBtn: true,
        timeout,
        progress: true
      })
    },
    notifySuccess (message) {
      this.$q.notify({
        icon: 'done',
        message,
        position: 'top',
        color: 'positive'
      })
    }
  }
}
