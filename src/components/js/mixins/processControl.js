export default {
  methods: {
    startProcess (ms = 30000) {
      this.$q.loading.show({
        message: this.$t('process.wait')
      })
      const _self = this
      const timeout = setTimeout(_self.somethingWrong, ms)
      this.$timeout = timeout
    },
    endProcess () {
      if (this.somethingWrongDialog) {
        this.somethingWrongDialog.hide()
        delete this.somethingWrongDialog
        this.$q.notify({
          icon: 'done',
          message: this.$t('process.done'),
          position: 'top',
          color: 'positive'
        })
      } else {
        this.$q.loading.hide()
        clearTimeout(this.$timeout)
        delete this.$timeout
      }
    },
    // Algo va mal
    somethingWrong () {
      this.$q.loading.hide()
      this.somethingWrongDialog = this.$q.dialog({
        title: this.$t('process.error.title'),
        message: this.$t('process.error.text'),
        persistent: true
      }).onOk(() => {
        location.reload()
      })
    }
  },

  beforeRouteLeave (to, from, next) {
    // Prevent back while es processing...
    if (this.$q.loading.isActive && this.$timeout) {
      this.$q.notify(this.$t('process.working'))
      // alert()
      next(false)
    } else {
      next()
    }
  }
}
