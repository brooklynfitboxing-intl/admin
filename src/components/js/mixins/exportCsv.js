import { exportFile } from 'quasar'

function wrapCsvValue (val, formatFn) {
  let formatted = formatFn !== void 0
    ? formatFn(val)
    : val
  formatted = formatted === void 0 || formatted === null
    ? ''
    : String(formatted)
  formatted = formatted.split('"').join('""')
  /**
   * Excel accepts \n and \r in strings, but some other CSV parsers do not
   * Uncomment the next two lines to escape new lines
   */
  // .split('\n').join('\\n')
  // .split('\r').join('\\r')
  return `"${formatted}"`
}
export default {
  methods: {
    exportTable (listName = 'list', fileName = 'export') {
      // naive encoding to csv format
      const gridColumns = this.columns.filter(col => this.visibleColumns.indexOf(col.name) > -1)
      const content = [ gridColumns.map(col => wrapCsvValue(col.label)) ].concat(
        this[listName].map(row => gridColumns.map(col => wrapCsvValue(
          typeof col.field === 'function'
            ? col.field(row)
            : row[col.field === void 0 ? col.name : col.field],
          col.format
        )).join(','))
      ).join('\r\n')
      const status = exportFile(
        `${fileName}.csv`,
        content,
        'text/csv'
      )
      if (status !== true) {
        this.notifyError('Browser denied file download...')
      }
    }
  }
}
