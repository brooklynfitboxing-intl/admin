function isObject (obj) {
  if (obj instanceof Date || obj instanceof String) return false
  return (typeof obj === 'object' && obj !== null) || typeof obj === 'function'
}

export default {
  data () {
    return {
      statePath: this.$route.path,
      stateAlreadySaved: false,
      stateChildren: {},
      stateInterrupSave: false
    }
  },
  methods: {
    async setState () {
      // console.log('State to Save', this.stateToSave, this.stateInterrupSave)
      if (!this.stateToSave) return
      if (this.stateInterrupSave) return
      const page = this.statePath
      const dataToSave = this.stateToSave
      const pageState = {}

      for (const key of dataToSave) {
        let value
        if (key.search(':') > 0) {
          let arrayKey = key.split(':')
          // State de un componente
          let componentRef = arrayKey[0]
          let component = this.$refs[componentRef]
          if (component) {
            value = component[arrayKey[1]]
          }
        } else {
          value = this[key]
        }
        pageState[key] = value
      }
      // console.log('setState', page, pageState)
      await this.$store.dispatch('app/savePageState', {
        page,
        data: pageState
      })
    },

    async updateState (data) {
      if (!data) return
      const page = this.statePath
      const pageState = this.$store.getters['app/pageState'][page] || {}
      // console.log('update state', data)
      await this.$store.dispatch('app/savePageState', {
        page,
        data: {
          ...pageState,
          ...data
        }
      })
    },

    getState () {
      if (!this.stateToSave) return
      const page = this.statePath
      const pageState = this.$store.getters['app/pageState'][page]

      // console.log('Getting State: ', page, pageState)

      for (const key in pageState) {
        if (key.search(':') > 0) {
          let arrayKey = key.split(':')
          if (!this.stateChildren[arrayKey[0]]) this.stateChildren[arrayKey[0]] = []
          this.stateChildren[arrayKey[0]].push(arrayKey[1])
        } else {
          let value = pageState[key]
          this[key] = isObject(value) ? { ...value } : value
        }
      }
    },

    getChildrenState () {
      const page = this.statePath
      const pageState = this.$store.getters['app/pageState'][page]
      // console.log('Children State', this.stateChildren)

      for (const child in this.stateChildren) {
        let component = this.$refs[child]
        if (component) {
          let keys = this.stateChildren[child]
          for (const key of keys) {
            let value = pageState[`${child}:${key}`]
            component[key] = isObject(value) ? { ...value } : value
          }
        }
      }
    },

    removeState () {
      this.$store.dispatch('app/removePageState', this.$route.path)
    },

    goBack () {
      this.$router.back()
    }
  },

  beforeRouteLeave (to, from, next) {
    // console.log('beforeLeave')
    this.setState()
    this.stateAlreadySaved = true
    next()
  },

  beforeMount () {
    this.getState()
  },

  created () {
    if (this.onSwitchCenter) this.$root.$on('switchCenter', this.onSwitchCenter)
    else if (this.refresh) this.$root.$on('switchCenter', this.refresh)
    else this.$root.$on('switchCenter', this.goBack)
  },

  mounted () {
    this.getChildrenState()
  },

  beforeDestroy () {
    // console.log('beforeDestroy')
    !this.stateAlreadySaved && this.setState()
    if (this.onSwitchCenter) this.$root.$off('switchCenter', this.onSwitchCenter)
    else if (this.refresh) this.$root.$off('switchCenter', this.refresh)
    else this.$root.$off('switchCenter', this.goBack)
  }
}
