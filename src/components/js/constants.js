const CLT_TYPES = {
  '0': '', '1': 'W', '2': '1', '3': 'D', '4': 'C', '5': 'M', '6': 'X'
}

const ORDER_STATUS = {
  'P': { name: '.Pending', color: 'accent' },
  'C': { name: 'Complete', color: 'state-5' },
  'F': { name: 'Failed', color: 'negative' }
}
const ORDER_TYPES = {
  'W': 'Web Offer',
  'R': 'Recurring',
  'F': 'Fitcoins',
  'S': 'Subscription',
  'O': 'Others',
  'Card': 'Add Credir Card'
}
const IMAGES_URI = 'https://www.brooklynfitzone.com/resources/images/'

const OTHER_LANGS = {
  'en': 'Inglés',
  'it': 'Italiano',
  'ru': 'Ruso',
  'pt': 'Portugués'
}

const KARMA_RANGE = {
  'B': {
    inc: 60,
    name: 'bronze'
  },
  'S': {
    inc: 70,
    name: 'silver'
  },
  'G': {
    inc: 80,
    name: 'gold'
  }
}

const H4C_HOUSES = {
  1: 'Children',
  2: 'Health',
  3: 'Forest',
  4: 'Oceanic'
}

const H4C_HOUSES_NAMES = {
  1: 'Casa de la Infancia',
  2: 'Casa de la Salud',
  3: 'Casa de los Bosques',
  4: 'Casa de los Océanos'
}

export {
  IMAGES_URI, CLT_TYPES, ORDER_TYPES, ORDER_STATUS, OTHER_LANGS, KARMA_RANGE,
  H4C_HOUSES, H4C_HOUSES_NAMES
}
