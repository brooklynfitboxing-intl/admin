import { date } from 'quasar'

function isToday (time) {
  return (new Date(time).toLocaleDateString() === new Date().toLocaleDateString())
}

function isYesterday (time) {
  return (date.addToDate(new Date(time), { days: 1 }).toLocaleDateString() === new Date().toLocaleDateString())
}

function adjustTimetableTime (theTime) {
  const time = new Date(theTime)
  // Correcion de fechas
  const limit19 = date.buildDate({
    year: 2019, month: 10, date: 27, hours: 2
  })

  const limit20 = date.buildDate({
    year: 2020, month: 3, date: 29, hours: 2
  })
  const limit202 = date.buildDate({
    year: 2020, month: 10, date: 25, hours: 2
  })

  const limit21 = date.buildDate({
    year: 2021, month: 3, date: 28, hours: 2
  })
  const limit212 = date.buildDate({
    year: 2021, month: 10, date: 31, hours: 2
  })
  const limit22 = date.buildDate({
    year: 2022, month: 3, date: 27, hours: 2
  })

  let hours = 1
  // if (time >= limit21) hours = 1
  if (time < limit19) {
    hours = 2
  } else if (time > limit20 && time < limit202) {
    hours = 2
  } else if (time > limit21 && time < limit212) {
    hours = 2
  } else if (time > limit22) {
    hours = 2
  }

  const adjustTime = date.addToDate(time, { hours })

  // Formatear Hora
  const arrDate = adjustTime.toISOString().split('T')[0].split('-')
  let arrTime = adjustTime.toISOString().split('T')[1].split(':')
  arrTime.pop()

  const localTime = date.buildDate(
    {
      year: parseInt(arrDate[0]),
      month: parseInt(arrDate[1]),
      date: parseInt(arrDate[2]),
      hours: parseInt(arrTime[0]),
      minutes: parseInt(arrTime[1]),
      seconds: 0
    }
  )

  return {
    localTime,
    hour: arrTime.join(':')
  }
}

function dateIsValid (date) {
  // Test which seperator is used '/' or '-'
  const opera1 = date.split('/')
  const opera2 = date.split('-')
  const lopera1 = opera1.length
  const lopera2 = opera2.length
  // Extract the string into month, date and year
  var pdate
  if (lopera1 > 1) {
    pdate = date.split('/')
  } else if (lopera2 > 1) {
    pdate = date.split('-')
  }
  // default format dd/mm/yyyy
  var dd = parseInt(pdate[0])
  var mm = parseInt(pdate[1])
  var yy = parseInt(pdate[2])

  if (pdate[0].length === 4) {
    // yyyy/mm/dd
    dd = parseInt(pdate[2])
    yy = parseInt(pdate[0])
  }

  // Create list of days of a month [assume there is no leap year by default]
  var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  if (mm === 1 || mm > 2) {
    return dd <= ListofDays[mm - 1]
  } else if (mm === 2) {
    var lyear = false
    if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
      lyear = true
    }
    if ((lyear === false) && (dd >= 29)) {
      return false
    }
    if ((lyear === true) && (dd > 29)) {
      return false
    }
    return true
  }
}

export {
  adjustTimetableTime,
  dateIsValid,
  isToday, isYesterday
}
