import enUS from './en-us.json'
import esES from './es-es.json'
import ru from './ru-ru.json'
import it from './it-it.json'

export default {
  'en-us': enUS,
  'es-es': esES,
  'ru-ru': ru,
  'it-it': it
}
