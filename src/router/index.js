import Vue from 'vue'
import VueRouter from 'vue-router'

const routes = [
  {
    path: '/districts',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Districts.vue') }
    ]
  },
  {
    path: '/districts/:id',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      title: 'District',
      back: true,
      dark: true
    },
    children: [
      { path: '', component: () => import('pages/ClubProfile.vue') }
    ]
  },
  {
    path: '/users/:id',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      title: 'User',
      back: true,
      dark: true
    },
    children: [
      { path: '', component: () => import('pages/UserProfile.vue') }
    ]
  },
  {
    path: '/users',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Users.vue') }
    ]
  },
  {
    path: '/fitboxers',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Fitboxers.vue') }
    ]
  },
  {
    path: '/redisKeys',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/RedisKeys.vue') }
    ]
  },
  {
    path: '/queues',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Queues.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/Blank.vue'),
    children: [
      { path: '', component: () => import('pages/Login.vue') }
    ]
  },

  {
    path: '/loading',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Loading.vue') }
    ]
  },
  {
    path: '/orders',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Orders.vue') }
    ]
  },
  {
    path: '/wallet',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Wallet.vue') }
    ]
  },
  {
    path: '/orders/:id',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      title: 'Order Detail',
      back: true,
      dark: true
    },
    children: [
      { path: '', component: () => import('pages/OrderDetail.vue') }
    ]
  },
  {
    path: '/campaing_logs',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/CampaingLogs.vue') }
    ]
  },
  {
    path: '/seasons/:id',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      title: 'Season Detail',
      back: true,
      dark: true
    },
    children: [
      { path: '', component: () => import('pages/SeasonDetail.vue') }
    ]
  },
  {
    path: '/challenges',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Challenges.vue') }
    ]
  },
  {
    path: '/challenges/:id',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      title: 'Challenge',
      back: true,
      dark: true
    },
    children: [
      { path: '', component: () => import('pages/ChallengeDetail.vue') }
    ]
  },
  {
    path: '/tournaments',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Tournaments.vue') }
    ]
  },
  {
    path: '/tournaments/:id',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      title: 'Tournament',
      back: true,
      dark: true
    },
    children: [
      { path: '', component: () => import('pages/TournamentDetail.vue') }
    ]
  },
  {
    path: '/combats/:id',
    component: () => import('layouts/MyLayout.vue'),
    meta: {
      title: 'Combat',
      back: true,
      dark: true
    },
    children: [
      { path: '', component: () => import('pages/CombatDetail.vue') }
    ]
  },
  {
    path: '/teams',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Teams.vue') }
    ]
  },
  {
    path: '/fitboxersFwg',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/FitboxersFwg.vue') }
    ]
  },
  {
    path: '/trainersFwg',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/TrainersFwg.vue') }
    ]
  },
  {
    path: '/Hits',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Hits.vue') }
    ]
  },
  {
    path: '/refereesFwg',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/RefereesFwg.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior (to, from, savedPosition) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (savedPosition) {
            resolve(savedPosition)
          } else {
            resolve({ x: 0, y: 0 })
          }
        }, 300)
      })
    },
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  return Router
}
