import VueApollo from 'vue-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { setContext } from 'apollo-link-context'
import { LocalStorage, Notify } from 'quasar'

const urlAdmin = process.env.adminService
const urlFwg = process.env.fwgService
// HTTP connection to the API
const httpLink = createHttpLink({
  uri: urlAdmin || 'https://admin.brooklynfitzone.com/api'
})
const httpLinkFwg = createHttpLink({
  uri: urlFwg || 'https://brooklynfitzone.com/fwg'
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token from cookies if it exists
  const token = LocalStorage.getItem('SessionHandle')
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache
})
const apolloClientFwg = new ApolloClient({
  link: authLink.concat(httpLinkFwg),
  cache
})

let apolloProvider = {}

export default ({ app, router, Vue }) => {
  Vue.use(VueApollo)

  // Build apollo provider
  apolloProvider = new VueApollo({
    defaultClient: apolloClient,
    clients: { 'fwg': apolloClientFwg, 'fitzone': apolloClient },
    errorHandler ({ graphQLErrors, networkError }) {
      if (graphQLErrors && graphQLErrors.length > 0) {
        graphQLErrors.map(({ message, locations, path }) => {
          // Mostrar Error
          if (message) {
            Notify.create({
              icon: 'error',
              message,
              position: 'top',
              color: 'negative'
            })
            if (message.toLowerCase() === 'not authorised!') {
              // redirect to login
              router.push('/login')
            }
          }
        })
      } else if (networkError) {
        // console.log('NetworkError', JSON.stringify(networkError))
        Notify.create({
          icon: 'error',
          message: typeof networkError === 'string' ? networkError : 'Network Error',
          position: 'top',
          color: 'negative'
        })
      }
    }
  })

  app.apolloProvider = apolloProvider
}

export {
  apolloProvider
}
