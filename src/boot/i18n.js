// import { Quasar, LocalStorage } from 'quasar'
// import VueI18n from 'vue-i18n'
// import messages from 'src/i18n'

// let i18nInstance

// export default async ({ app, Vue }) => {
//   Vue.use(VueI18n)

//   let langIso = LocalStorage.getItem('lang')

//   if (!langIso) {
//     langIso = Quasar.lang.getLocale() // returns a string
//   }

//   let quasarLangIso = langIso
//   switch (quasarLangIso) {
//     case 'es-es':
//       quasarLangIso = 'es'
//       break
//     case 'it-it':
//       quasarLangIso = 'it'
//       break
//     case 'ru-ru':
//       quasarLangIso = 'ru'
//       break
//     default:
//       quasarLangIso = 'en-us'
//   }

//   try {
//     await import(`quasar/lang/${quasarLangIso}`)
//       .then(lang => {
//         Quasar.lang.set(lang.default)
//       })
//     // Set i18n instance on app
//     app.i18n = new VueI18n({
//       locale: langIso,
//       fallbackLocale: 'en-us',
//       messages
//     })
//     i18nInstance = app.i18n
//   } catch (err) {
//     // Requested Quasar Language Pack does not exist,
//     // let's not break the app, so catching error
//     // console.error(err)
//   }
// }

// export { i18nInstance }
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from 'src/i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'en-us',
  fallbackLocale: 'en-us',
  messages
})

export default ({ app }) => {
  // Set i18n instance on app
  app.i18n = i18n
}

export { i18n }
