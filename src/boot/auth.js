import { LocalStorage } from 'quasar'

export default ({ app, router, store, Vue }) => {
  /*
   * Provisional Role
   */
  const roleInfo = LocalStorage.getItem('currentRole')
  if (roleInfo) {
    store.dispatch('auth/setRole', roleInfo)
  }

  const user = LocalStorage.getItem('user')
  if (roleInfo) {
    store.dispatch('auth/setUser', user)
  }

  const isLogged = () => store.getters['auth/loggedIn']
  const getRole = () => store.getters['auth/currentRole']
  const getPermissions = () => store.getters['auth/user'].permissions

  const isAdmin = () => getPermissions().filter(el => el.role === 'ADMIN').length > 0

  router.beforeEach((to, from, next) => {
    const auth = to.matched.find(record => record.meta.auth)
    const roleRecord = to.matched.find(record => record.meta.role)
    if (auth) {
      if (!isLogged) {
        router.push('/login')
      }
      if (isAdmin()) {
        return next()
      }
      // check boy Role
      if (!roleRecord) return next()

      const role = roleRecord.meta.role
      const myRole = getRole().role
      if (role && myRole && role.indexOf(myRole) === -1) {
        next('not_authorized')
      } else {
        next()
      }
    }
    next()
  })

  router.beforeResolve((to, from, next) => {
    if (to.fullPath === '/') {
      next('/districts')
    }
    next()
  })

  /*
   * Roles & Permisions
   */
  const authHelper = {}
  authHelper.login = (data) => store.dispatch('auth/login', data)
  authHelper.isLogged = isLogged

  authHelper.role = getRole
  authHelper.isAdmin = isAdmin

  authHelper.permissions = getPermissions
  authHelper.canSwitchRole = () => store.getters['auth/user'].permissions && store.getters['auth/user'].permissions.length > 1

  const profileHelper = {}
  profileHelper.avatar = () => store.getters['auth/user'].avatar
  profileHelper.name = () => store.getters['auth/user'].name
  profileHelper.email = () => store.getters['auth/user'].email
  profileHelper.center = () => (store.getters['auth/currentRole'].district || {}).alias
  profileHelper.load = () => {

  }

  Vue.prototype.$auth = authHelper
  Vue.prototype.$profile = profileHelper
  Vue.prototype.$prevRoute = {}
}
