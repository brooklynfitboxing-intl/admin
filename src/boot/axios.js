import axios from 'axios'
import { LocalStorage } from 'quasar'

const session = LocalStorage.getItem('SessionHandle')
const axiosInstance = axios.create({
  baseURL: process.env.authUrl || ''
})

if (session) {
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${session}` // for all requests
}

const iotInstance = axios.create({
  baseURL: process.env.iotUrl || 'https://bfi.eu.ngrok.io/iot/iot/'
})

export default ({ Vue }) => {
  Vue.prototype.$axios = axiosInstance
  Vue.prototype.$iot = iotInstance
}

export { axiosInstance }
