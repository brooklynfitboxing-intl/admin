import { LocalStorage } from 'quasar'

import { axiosInstance } from 'boot/axios'

function saveCredentialsInLocal (user, currentRole) {
  LocalStorage.set('currentRole', {
    role: currentRole.role,
    district: {
      alias: currentRole.district.alias
    }
  })
  const { avatar, email, name } = user
  LocalStorage.set('user', {
    avatar, email, name
  })
}

export function login (state, payload) {
  return axiosInstance.post('/user/login', payload)
    .then((response) => {
      const { data } = response

      if (typeof data === 'string') {
        // Error message
        throw new Error(data)
      }

      const currentRole = data.permissions.find(
        el => el.role === 'ADMIN'
      )

      if (currentRole) {
        LocalStorage.set('SessionHandle', data.token)
        axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${data.token}` // for all requests

        state.commit('setUser', data)
        state.commit('setRole', currentRole)

        saveCredentialsInLocal(data, currentRole)
        LocalStorage.set('verifiedRole', true)

        const fitzoneUri = currentRole.district.server.fitzoneUri || 'http://localhost/fs1'
        LocalStorage.set('fitzoneService', fitzoneUri)
      } else {
        throw new Error('Not Authorised!. You must be Admin')
      }
    })
}

export function setRole (state, payload) {
  state.commit('setRole', payload)
}

export function setUser (state, payload) {
  state.commit('setUser', payload)
}

export function setAvatar (state, uri) {
  state.commit('setAvatar', uri)
}

export function verify (state) {
  const verifiedRole = LocalStorage.getItem('verifiedRole')
  if (verifiedRole) {
    LocalStorage.remove('verifiedRole')
    return
  }
  return axiosInstance.get('/user/verify')
    .then((response) => {
      const { data } = response
      const currentRole = data.permissions.find(
        el => el.id === data.currentRole
      )

      state.commit('setUser', data)
      state.commit('setRole', currentRole)

      saveCredentialsInLocal(data, currentRole)
    })
}

export function logout (state) {
  return axiosInstance.post('/user/logout')
    .then(() => {
      LocalStorage.remove('SessionHandle')
      LocalStorage.remove('currentRole')
      LocalStorage.remove('user')
      state.commit('setUser', {})
      state.commit('setRole', {})
    })
}
