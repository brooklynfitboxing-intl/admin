export function setUser (state, data) {
  state.user = data
}

export function setRole (state, data) {
  state.currentRole = data
}

export function setRoleCenter (state, data) {
  state.currentRole.district = data
}

export function setAvatar (state, uri) {
  state.user.avatar = uri
}
