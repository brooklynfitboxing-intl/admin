import { LocalStorage } from 'quasar'

export function user (state) {
  return state.user || {
    avatar: 'man_ico.png',
    name: 'Unknown',
    email: ''
  }
}

export function loggedIn (state) {
  if (state.user) {
    return true
  } else {
    const token = LocalStorage.getItem('SessionHandle')
    return token
  }
}

export function currentRole (state) {
  if (state.currentRole) {
    return state.currentRole
  } else {
    return {
      district: {
        alias: 'Unknown'
      }
    }
  }
}
