export function pageState (state) {
  return state.pages
}

export function flashData (state) {
  return state.flashData
}
