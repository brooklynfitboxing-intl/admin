export function savePageState (state, payload) {
  state.commit('setPageState', payload)
}

export function removePageState (state, name) {
  state.commit('removePageState', name)
}
