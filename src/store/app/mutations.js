export function setPageState (state, { page, data }) {
  state.pages[page] = data
}

export function removePageState (state, name) {
  delete state.pages[name]
}

export function setFlashData (state, { path, data }) {
  state.flashData[path] = data
}

export function removeFlashData (state) {
  state.flashData = null
}
