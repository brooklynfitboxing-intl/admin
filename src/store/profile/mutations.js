
export function setProfile (state, payload) {
  const { clt_tip: cltTip, karma_balance: karma, fitcoins, id } = payload
  state.id = id
  state.fitboxerType = cltTip
  state.karma = karma
  state.fitcoins.total = fitcoins.reduce((acc, it) => acc + it.dis, 0)
}

export function setFitcoins (state, { operation, value }) {
  if (operation === '-') {
    state.fitcoins.total -= value
  } else if (operation === '+') {
    state.fitcoins.total += value
  } else {
    state.fitcoins.total = value
  }
}

export function setFitboxerType (state, type) {
  state.fitboxerType = type
}

export function setKarma (state, { karma, ope }) {
  switch (ope) {
    case '+':
      state.karma += karma
      break
    case '-':
      state.karma -= karma
      break
    default:
      state.karma = karma
  }
}
