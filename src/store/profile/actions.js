// import { fitzoneInstance } from 'boot/axios'

export function load (state, data) {
  state.commit('setProfile', data)
  /*
  return fitzoneInstance.get('/app/load_profile')
    .then((response) => {
      state.commit('setProfile', response.data)
    })
  */
}

export function setFitcoins (state, payload) {
  state.commit('setFitcoins', payload)
}

export function setFitboxerType (state, type) {
  state.commit('setFitboxerType', type)
}

export function setKarma (state, payload) {
  state.commit('setKarma', payload)
}
